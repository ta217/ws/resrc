import os

class Rule:

    sd_props = [
        "CPUQuota", "MemoryMax"
    ]

    custom_props = []

    def __init__(self, name="", rules={}):
        self.cpu_cnt = int(os.sysconf('SC_NPROCESSORS_CONF'))
        self.ram_bytes = int(os.sysconf('SC_PAGE_SIZE') * os.sysconf('SC_PHYS_PAGES'))
        self.rules = rules
        self.name = name
        if self.check_rules(rules):
            raw_rules = [self.translate_rule(n,v) for (n,v) in rules.items()]
            self.rules = raw_rules
        else:
            pass # won't be here

    @staticmethod
    def percent(pstr):
        return 0.01 * float(pstr.strip("%"))

    def check_rules(self, rules):
        for (n,v) in rules.items():
            if (not n in self.sd_props) and (not n in self.custom_props):
                quit("Property \"{0}\" does not exist in resrc".format(n))
        return True

    def translate_rule(self, name, v):
        p = self.percent(v)
        if "Memory" in name and "%" in v:
            return "{0}={1}".format(name, int(self.ram_bytes * p))
        return "{0}={1}".format(name,v)

    def get(self):
        return self.rules

class RuleEntry:

    def __init__(self, rules, users):
        self.rules = rules
        self.users = users

    def match(self, uid=-1, gid=-1):
        return self.users.match(uid,gid)

    def get_rules(self):
        return self.rules
