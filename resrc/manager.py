# Copyright 2017 Yunchih Chen <yunchih@csie.ntu.edu.tw>

import logging, re, sys, select, subprocess
from systemd import login
from resrc.utils import quit
from resrc.users import Users

class Systemctl:

    def ListUnits(self):
        return subprocess.run(["systemctl", "list-units"], timeout=10, capture_output=True).stdout.decode().split()

    def apply(self, unit, p):
        subprocess.run(["systemctl", "set-property", "--runtime", unit, p], timeout=10)


class UsersResourceManager:

    def __init__(self, ruleset=[], dry_run=False, apply_existing=False):
        self.ruleset = ruleset
        self.dry_run = dry_run
        self.processed_user = set()

        if len(ruleset) == 0:
            quit("No rules installed!")

        self.sd_manager = Systemctl()

        if apply_existing:
            unit_list = self.sd_get_unit_list()
            uids = self.sd_get_uids_from_units(unit_list)

            for _uid in uids:
                uid, gid = 0, 0

                try:
                    uid = int(_uid)
                    gid = Users.get_user_gid(uid)
                    logging.info("Existing user detected: %d" % uid)
                except KeyError:
                    # just in case
                    logging.error("Existing user detected but uid not found: %d" % uid)

                self.apply_rule(uid, gid)

    def monitor_new_user(self):
        logging.info("Start monitoring incoming users ...")
        while True:
            m = login.Monitor()
            p = select.poll()
            p.register(m, m.get_events())
            p.poll()

            for uid in login.uids():
                if uid < 1000 or uid in self.processed_user:
                    continue

                gid = 0
                try:
                    gid = Users.get_user_gid(uid)
                    logging.info("New user detected: %d" % uid)
                except Exception as e:
                    logging.error("New user detected but uid not found: %d" % uid)
                    continue

                self.apply_rule(uid, gid)

    def apply_rule(self, uid, gid):
        if not uid or not gid: # exclude root and malformed uid, gid
            return

        for entry in self.ruleset:
            # Only apply the first matched rule
            if entry.match(uid, gid):
                logging.info("UID {0} matches rule \"{1}\"".format(uid, entry.get_rules().name))
                r = entry.get_rules().rules
                if (not self.dry_run) and r:
                    self.sd_set_unit_properties(uid, r)
                break
        self.processed_user.add(uid)

    def sd_set_unit_properties(self, uid, properties):
        sd_unit = "user-%d.slice" % uid

        try:
            for p in properties:
                self.sd_manager.apply(sd_unit, p)
                logging.info("Resource limitation imposed on user: UID {0} PROPERTY {1}".format(uid, p))
        except Exception as e:
            logging.error("Failed imposing resource limit on %d: %s" % (uid, e))

    def sd_get_unit_list(self):
        try:
            return self.sd_manager.ListUnits()
        except Exception as e:
            logging.error("Failed getting all units: %s" % e)
        return []

    def sd_get_uids_from_units(self, units):
        r = re.compile("^user-[0-9]*\.slice")
        r2 = re.compile("[0-9]+")
        user_slices = [u for u in units if re.match(r, u)]
        return [re.search(r2, u).group(0) for u in user_slices]
